<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session;
use App;
use Illuminate\Support\Facades\Config;

class GetLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->has('locale')){
            $locale = session()->get('locale', Config::get('app.locale'));
        }else{
            $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
            if ($locale != 'fr' && $locale != 'en' && $locale != 'am' ) {
                $locale = 'en';
            }elseif ($locale == 'fr'){
                $locale = 'fr';
            }elseif ($locale == 'en'){
                $locale = 'en';
            }elseif ( $locale == 'am'){
                $locale = 'am';
            }
        }
        app()->setLocale($locale);
        return $next($request);
    }
}
