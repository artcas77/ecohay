<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function contactUs()
    {
        return view('frontend.pages.contact');
    }


    public function postContactUs(Request $request)
    {

        $this->validate($request , [
            'contact_message' => 'required',
            'contact_name' => 'required',
            'contact_email' => 'required',
            'contact_phone' => 'required'
        ]);


        $input = $request->except('_token');

        Contact::create($input);

        session()->flash('success' , __('contact.success'));

        return redirect()->back();
    }

}
