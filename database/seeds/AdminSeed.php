<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\User;
class AdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('users')){
            User::create([
                'name' => 'Admin',
                'email' => 'admin@ecohay.com',
                'password' => bcrypt('superadmin'),
                'role' => 1
            ]);
        }
    }
}
