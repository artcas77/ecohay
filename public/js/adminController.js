$(document).ready(function () {

    $(".delete").click(function () {

        var url = $(this).data("url");

        $(".delete_form").attr('action',url);



    })
    $(".stop").on('click', function () {
        $(".name").each(function () {
            if ($(this).val() == '') {
                $(".stop").attr('type', 'button');
                 $error = '<strong>The name field is required.</strong>';
                $(this).next().html($error);

            } else {

                $(".stop").attr('type', 'submit');
            }
        });
    });

    $('.updateBlogCategory').click(function () {

        var url = $(this).data("url");

        var am  = $(this).data().namear;

        var ru  = $(this).data().nameru;

        var en  = $(this).data().nameen;

        $('#name_arm').val(am);

        $('#name_ru').val(ru);

        $('#name_en').val(en);

        $('.update').attr('action', url);


    });

    $(".deleteBlogtag").click(function () {
        var id = $(this).data().id;
        var url = $(this).data().url;
        var tag = $(this);
        $('.delete_form').attr('action', url);

    });


    $('.updateBlogTag').click(function () {

        var url = $(this).data("url");

        var am  = $(this).data().namear;

        var ru  = $(this).data().nameru;

        var en  = $(this).data().nameen;

        $('#name_arm').val(am);

        $('#name_ru').val(ru);

        $('#name_en').val(en);

        $('.update').attr('action', url);

    });

    $(".deleteBlog").click(function () {
        var id = $(this).data().id;
        var url = $(this).data().url;
        var tag = $(this);
        $('.delete_form').attr('action', url);

    });

    $(".deleteGallery").click(function () {
        var id = $(this).data().id;
        var url = $(this).data().url;
        var tag = $(this);
        console.log(id);
        console.log(url);
        console.log(tag);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: "DELETE",
            data: {id: id},
            success: function (result) {
                if (result['status'] === "true") {
                    console.log(result['message']);
                    tag.parent().parent().remove();
                } else {
                    console.log(result['message']);
                }
            }
        })
    });
    $(".deleteGalleryItem").click(function () {
        var id = $(this).data().id;
        var url = $(this).data().url;
        var tag = $(this);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: "post",
            data: {id: id},
            success: function (result) {
                if (result['status'] === "true") {
                    console.log(result['message']);
                    tag.parent().parent().remove();
                } else {
                    console.log(result['message']);
                }

            }
        })
    });
    $(".deletecontacts").click(function () {

        var id = $(this).data().id;
        var url = $(this).data().url;
        var tag = $(this);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            method: "DELETE",
            data: {id: id},
            success: function (result) {
                if (result['status'] === "true") {
                    console.log(result['message']);
                    tag.parent().parent().remove();
                } else {
                    console.log(result['message']);
                }

            }
        })
    });




    $(".updatess").click(function () {
        $(".updates").each(function () {
            if ($(this).val() === '') {
                $(".updatess").attr('type', 'button');
                $error = '<strong>The name field is required.</strong>';
                $(this).next().html($error);

            } else {

                $(".updatess").attr('type', 'submit');
            }
        });
    });

    $('#upload_btn').click(function () {
        $('#fileToUpload').click();
    });

    $('#fileToUpload').change(function () {
        $('#print_name').text($(this)[0].files[0].name).show()
    });
    $('#upload_files').click(function () {
        $('#files_to_upload').click();
    });

    $('#files_to_upload').change(function () {
        var array = ($(this)[0].files);
        array = [...array];
        var imagesNames = "";
        array.forEach(item =>  imagesNames +=  item.name + ", ");
        $('#print_names').text(imagesNames).show();
    });
    $('.radio').change(function () {
        if ($("input:checked").val() === "image") {
            $('.image').removeClass('displayNone');
            $('.video').addClass('displayNone');
            $('#video').empty();
            $('#video').val(" ");

        }
        else if ($(this).val() === "Video") {
            $('.image').addClass('displayNone');
            $('.video').removeClass('displayNone');
            $('#fileToUpload').empty();
            $('#print_name').empty();
        }

    });




    $('#datas').DataTable({
        responsive: true,
        order: [[ 0, 'desc' ], [ 1, 'desc' ]]
    });


    $("").change(function () {
        alert("ok");
        var id = $(this).data().id;
        var url = $(this).data().url;
        var tag = $(this);

    });






});

$('#Services').on('change', function() {

    var id =  this.value
    var url = $(this).data().url;

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: "post",
        data: {id: id},
        success: function (result) {
            const arrNames = result.map(el => el.name_en)

            const arrIDs = result.map(el => el.id);

            document.getElementById('sel1').innerHTML = '';

            for (let i = arrNames.length-1; i>=0; i--){
                document.getElementById('sel1').insertAdjacentHTML('afterbegin', `<option value="${arrIDs[i]}">${arrNames[i]}</option>`)
            }
        }
    })

});



