<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();

/**==========================Language Change Routes ================================*/

Route::get('setlang/{lang}', function($lang)
{
    session()->put('locale', $lang);

    app()->setLocale($lang);
    return  redirect()->back();
})->name('setlang');

Route::get('/home', 'HomeController@index')->name('home');

/*--------------------------Frontend Routes ---------------------------------*/
Route::get('/', function () {
    return view('frontend.pages.home');
});

Route::get('contact_us' , 'HomeController@contactUs')->name('contactUs');
Route::post('contact_us' , 'HomeController@postContactUs')->name('postContactUs');


/*-------------------------- End Frontend Routes ---------------------------------*/


/*--------------------------Admin Routes ---------------------------------*/
Route::get('/', function () {
    return view('frontend.pages.home');
});



/*-------------------------- End Admin Routes ---------------------------------*/

Route::group(['namespace' => 'Admin' , 'prefix' => 'admin'] , function(){
    Route::get('/login' , 'AdminController@login')->name('admin.login');
    Route::post('/login' , 'AdminController@postLogin')->name('admin.postLogin');
    Route::group(['middleware' => 'admin'], function() {
        Route::get('/', 'AdminController@dashboard')->name("admin.dashboard");


        Route::resources([
            'adminContacts' => 'ContactsController',
            'adminCategories' => 'CategoriesController',
            'products' => 'ProductsController',
        ]);
    });
});
