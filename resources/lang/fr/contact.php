<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Contact US  Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'write_us' => 'test',
    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone Number',
    'whats' => 'What`s on your mind',
    'submit' => 'Submit',
    'contact_details' => 'Contact details'

];
