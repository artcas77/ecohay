<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Contact US  Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'write_us' => 'Գրեք Մեզ',
    'name' => 'Անուն',
    'email' => 'էլ հասցե',
    'phone' => 'Հեռախոսահամար',
    'whats' => 'Ձեր հարցերը',
    'submit' => 'ուղղարկել',
    'contact_details' => 'Մեր Տվյալները'

];
