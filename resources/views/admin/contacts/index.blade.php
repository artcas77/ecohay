@extends('admin.layouts.layout')

@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.5.1/r-2.2.1/datatables.min.css"/>
    <link rel="stylesheet" href="{{ asset('css/toastr.css') }}">
@endsection

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12  col-12 mb-2">
                    <h3 class="content-header-title mb-0">Contacts Massages List</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route("admin.dashboard")}}">Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <table class="table " id="datas">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>View</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if($contacts->first())
                                                    @foreach($contacts as $contact)
                                                        <tr>
                                                            <td> {{ $contact->contact_name	 }} </td>
                                                            <td> {{ $contact->contact_email	 }} </td>
                                                            <td> {{ $contact->contact_phone	 }} </td>
                                                            <td>
                                                                <a href="{{ route('adminContacts.show' , ['id' => $contact->id]) }}">
                                                                    <i class="fa fa-eye-slash"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!--data Tables -->
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.5.1/r-2.2.1/datatables.min.js"></script>
    <!-- / data Tables -->
    <script>
        $('#datas').DataTable({
            responsive: true,
            order: [[ 0, 'desc' ], [ 1, 'desc' ]]
        });
    </script>
    <!-- Toastr js -->
    <script src="{{ asset('/js/toastr.min.js') }}"></script>
    <script>
        @if(Session::has('success'))
        toastr.success('{{ Session::get('success') }}', {"showMethod": "fadeIn", "hideMethod": "fadeOut", timeOut: 2000});
        @endif
    </script>
    <!--  /Toastr js -->
@endsection
