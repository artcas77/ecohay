@extends('admin.layouts.layout')



@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12  col-12 mb-2">
                    <h3 class="content-header-title mb-0">Contacts Massage</h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route("admin.dashboard")}}">Home</a>
                                </li>
                                <li class="breadcrumb-item">
                                    <a href="{{route("adminContacts.index")}}">Contacts List</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 text-center text-md-left">
                                            <p class="lead">Contact Information</p>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <table class="table table-borderless table-sm">
                                                        <tbody>
                                                        <tr>
                                                            <td>Name</td>
                                                            <td class="text-right">{{ $contact->contact_name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td class="text-right">{{ $contact->contact_email }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Phone</td>
                                                            <td class="text-right">{{ $contact->contact_phone }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Message</td>
                                                            <td class="text-right">{{ $contact->contact_message }}</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

