<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <title>EcoHay | Administration Area</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link
        href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/vendors.css")}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/app.css")}}">
    <!-- END STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/pages/login-register.css")}}">
    <!-- END Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/main.css")}}">
    <!-- END Custom CSS-->
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column"
      class=" backgraund vertical-layout vertical-menu 1-column  bg-cyan bg-lighten-2 menu-expanded fixed-navbar">
<!-- fixed-top-->

<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class=" col-12 col-sm-6 col-md-5 col-lg-4 m-auto box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 px-2 py-2 row mb-0">
                    <div class="card-header border-0">
                        <div class="card-title text-center">
                            <img src="{{asset('image/logo/logo.png')}}" alt="branding logo" width="100px">
                        </div>
                        <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                            <span>Login with Stack</span>
                        </h6>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form-horizontal" action="{{route("admin.postLogin")}}" method="post">
                                @csrf
                                <fieldset class="form-group position-relative has-icon-left">
                                    <input type="text"
                                           class="form-control input-lg {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" id="email" placeholder="Email"
                                           tabindex="1" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    <div class="form-control-position">
                                        <i class="ft-user"></i>
                                    </div>
                                    <div class="help-block font-small-3"></div>
                                </fieldset>
                                <fieldset class="form-group position-relative has-icon-left">
                                    <input type="password" class="form-control input-lg " name="password" id="password"
                                           placeholder="Enter Password"
                                           tabindex="2" required
                                           data-validation-required-message="Please enter valid passwords.">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <div class="form-control-position">
                                        <i class="fa fa-key"></i>
                                    </div>
                                    <div class="help-block font-small-3"></div>
                                </fieldset>
                                <div class="form-group row">
                                    <div class="col-md-6 col-12 text-center text-md-left">
                                        <fieldset>
                                            <input type="checkbox" id="remember-me" class="chk-remember">
                                            <label for="remember-me"> Remember Me</label>
                                        </fieldset>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-danger btn-block btn-lg"><i class="ft-unlock"></i>
                                    Login
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
</body>
</html>
