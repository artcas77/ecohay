<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
          rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/vendors.css ')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("app-assets/vendors/css/charts/morris.css")}}">
    {{--<link rel="stylesheet" type="text/css" href="{{asset(" app-assets/vendors/css/extensions/unslider.css")}}">--}}
    <link rel="stylesheet" type="text/css" href="{{asset("app-assets/vendors/css/weather-icons/climacons.min.css")}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/app.css")}}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/core/menu/menu-types/vertical-menu.css ")}}">
    <!-- link(rel='stylesheet', type='text/css', href='..'"/../../app-assets/css#{rtl}/pages/users.css')-->
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <title>Ecohay Administration Area</title>
    @yield("styles")

</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">
<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item">
                    <a href="{{ route('admin.dashboard') }}" class="navbar-brand">
                        <img alt="Ecohay Admin Logo" src="{{asset("image/logo/logo.png")}}" width="25px"
                             class="brand-logo">
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div id="navbar-mobile" class="collapse navbar-collapse">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu"></i></a></li>
                </ul>
                <ul class="nav navbar-nav float-right">

                    <li class="dropdown dropdown-user nav-item">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">

                            <span class="user-name">{{ Auth::user()->name }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-divider"></div><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"class="dropdown-item"><i class="ft-power"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class=" navigation-header">
                <span>General</span>
            </li>

            <li class=" nav-item"><a href=""> <i class="fa fa-anchor"></i><span data-i18n="" class="menu-title">Categories</span></a>
            <li class=" nav-item"><a href=""> <i class="fa fa-podcast"></i><span data-i18n="" class="menu-title">Products</span></a>
            <li class=" nav-item"><a href=""> <i class="fa fa-bank"></i><span data-i18n="" class="menu-title">Orders</span></a>
            </li>
            <li class=" nav-item"><a href="{{ route('adminContacts.index') }}"> <i class="fa fa-address-card-o"></i><span data-i18n="" class="menu-title">Contacts</span></a>
            </li>

        </ul>
    </div>
</div>

          @yield("content")

<!-- ////////////////////////////////////////////////////////////////////////////-->
<footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; {{ \Carbon\Carbon::now()->format('Y') }} <a href="https://genweb.am"
                                                                                     target="_blank" class="text-bold-800 grey darken-2">GENWEB </a>, All rights
        reserved. </span>
        <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
    </p>
</footer>
<!-- BEGIN VENDOR JS-->
<script src="{{asset("app-assets/vendors/js/vendors.min.js")}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{asset("app-assets/vendors/js/extensions/jquery.knob.min.js")}}" type="text/javascript"></script>
<script src="{{asset("app-assets/js/scripts/extensions/knob.js")}}" type="text/javascript"></script>
<script src="{{asset("app-assets/vendors/js/charts/raphael-min.js ")}}" type="text/javascript"></script>
<script src="{{asset("app-assets/vendors/js/charts/morris.min.js ")}}" type="text/javascript"></script>
<script src="{{asset("app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js")}}"
        type="text/javascript"></script>
<script src="{{asset("app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js ")}}"
        type="text/javascript"></script>
<script src="{{asset("app-assets/data/jvector/visitor-data.js ")}}" type="text/javascript"></script>
<script src="{{asset("app-assets/vendors/js/charts/chart.min.js")}}" type="text/javascript"></script>
<script src="{{asset("app-assets/vendors/js/charts/jquery.sparkline.min.js")}}" type="text/javascript"></script>
<script src="{{asset("app-assets/vendors/js/extensions/unslider-min.js")}}" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{asset("app-assets/css/core/colors/palette-climacon.css")}}">
<link rel="stylesheet" type="text/css" href="{{asset("app-assets/fonts/simple-line-icons/style.min.css")}}">
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{asset("app-assets/js/core/app-menu.js ")}}" type="text/javascript"></script>
<script src="{{asset("app-assets/js/core/app.js ")}}" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{asset('app-assets/js/scripts/pages/dashboard-ecommerce.min.js')}}" type="text/javascript"></script>
@yield('script');
<!-- END PAGE LEVEL JS-->
</body>
</html>
