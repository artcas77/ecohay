@extends('frontend.layout.app')

@section('title' , '')

@section('keywords' , '')

@section('description' , '')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-9 offset-lg-3">
                <div class="home-slider owl-carousel owl-carousel-lazy owl-theme">
                    <div class="home-slide">
                        <div class="owl-lazy slide-bg" src="frontend/images/slider/slide-1.jpg" data-src="frontend/images/slider/slide-1.jpg"></div>
                        <a href="category.html">
                        </a>
                    </div><!-- End .home-slide -->

                    <div class="home-slide">
                        <div class="owl-lazy slide-bg" src="frontend/images/slider/slide-2.jpg" data-src="frontend/images/slider/slide-2.jpg"></div>
                        <a href="category.html">
                        </a>
                    </div><!-- End .home-slide -->
                </div><!-- End .home-slider -->
            </div><!-- End .col-lg-9 -->
        </div><!-- End .row -->

        <div class="info-boxes-container">
            <div class="container-fluid">
                <div class="info-box">
                    <i class="icon-shipping"></i>

                    <div class="info-box-content">
                        <h4>FREE SHIPPING & RETURN</h4>
                        <p>Free shipping on all orders over $99.</p>
                    </div><!-- End .info-box-content -->
                </div><!-- End .info-box -->

                <div class="info-box">
                    <i class="icon-us-dollar"></i>

                    <div class="info-box-content">
                        <h4>MONEY BACK GUARANTEE</h4>
                        <p>100% money back guarantee</p>
                    </div><!-- End .info-box-content -->
                </div><!-- End .info-box -->

                <div class="info-box">
                    <i class="icon-support"></i>

                    <div class="info-box-content">
                        <h4>ONLINE SUPPORT 24/7</h4>
                        <p>Lorem ipsum dolor sit amet.</p>
                    </div><!-- End .info-box-content -->
                </div><!-- End .info-box -->
            </div><!-- End .container-fluid -->
        </div><!-- End .info-boxes-container -->

        <h2 class="title text-center">Featured Products</h2>
        <div class="row justify-content-center">
            <div class="col-6 col-md-4 col-lg-3">
                <div class="product">
                    <figure class="product-image-container">
                        <a href="product.html" class="product-image">
                            <img src="frontend/images/products/product-3.jpg" alt="product">
                        </a>
                        <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    </figure>
                    <div class="product-details">
                        <div class="ratings-container">
                            <div class="product-ratings">
                                <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                            </div><!-- End .product-ratings -->
                        </div><!-- End .product-container -->
                        <h2 class="product-title">
                            <a href="product.html">Brown Scarf</a>
                        </h2>
                        <div class="price-box">
                            <span class="product-price">$66.00</span>
                        </div><!-- End .price-box -->

                        <div class="product-action">
                            <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                                <span>Add to Wishlist</span>
                            </a>

                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Add to Cart</span>
                            </a>

                            <a href="#" class="paction add-compare" title="Add to Compare">
                                <span>Add to Compare</span>
                            </a>
                        </div><!-- End .product-action -->
                    </div><!-- End .product-details -->
                </div><!-- End .product -->
            </div><!-- End .col-lg-3 -->

            <div class="col-6 col-md-4 col-lg-3">
                <div class="product">
                    <figure class="product-image-container">
                        <a href="product.html" class="product-image">
                            <img src="frontend/images/products/product-8.jpg" alt="product">
                        </a>
                        <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    </figure>
                    <div class="product-details">
                        <div class="ratings-container">
                            <div class="product-ratings">
                                <span class="ratings" style="width:0%"></span><!-- End .ratings -->
                            </div><!-- End .product-ratings -->
                        </div><!-- End .product-container -->
                        <h2 class="product-title">
                            <a href="product.html">Yellow Highheels</a>
                        </h2>
                        <div class="price-box">
                            <span class="product-price">$55.00</span>
                        </div><!-- End .price-box -->

                        <div class="product-action">
                            <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                                <span>Add to Wishlist</span>
                            </a>

                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Add to Cart</span>
                            </a>

                            <a href="#" class="paction add-compare" title="Add to Compare">
                                <span>Add to Compare</span>
                            </a>
                        </div><!-- End .product-action -->
                    </div><!-- End .product-details -->
                </div><!-- End .product -->
            </div><!-- End .col-lg-3 -->

            <div class="col-6 col-md-4 col-lg-3">
                <div class="product">
                    <figure class="product-image-container">
                        <a href="product.html" class="product-image">
                            <img src="frontend/images/products/product-13.jpg" alt="product">
                        </a>
                        <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                        <span class="product-label label-sale">-20%</span>
                    </figure>
                    <div class="product-details">
                        <div class="ratings-container">
                            <div class="product-ratings">
                                <span class="ratings" style="width:60%"></span><!-- End .ratings -->
                            </div><!-- End .product-ratings -->
                        </div><!-- End .product-container -->
                        <h2 class="product-title">
                            <a href="product.html">Blue Shoes</a>
                        </h2>
                        <div class="price-box">
                            <span class="old-price">$101.00</span>
                            <span class="product-price">$89.00</span>
                        </div><!-- End .price-box -->

                        <div class="product-action">
                            <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                                <span>Add to Wishlist</span>
                            </a>

                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Add to Cart</span>
                            </a>

                            <a href="#" class="paction add-compare" title="Add to Compare">
                                <span>Add to Compare</span>
                            </a>
                        </div><!-- End .product-action -->
                    </div><!-- End .product-details -->
                </div><!-- End .product -->
            </div><!-- End .col-lg-3 -->

            <div class="col-6 col-md-4 col-lg-3">
                <div class="product">
                    <figure class="product-image-container">
                        <a href="product.html" class="product-image">
                            <img src="frontend/images/products/product-12.jpg" alt="product">
                        </a>
                        <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                        <span class="product-label label-new">New</span>
                    </figure>
                    <div class="product-details">
                        <div class="ratings-container">
                            <div class="product-ratings">
                                <span class="ratings" style="width:40%"></span><!-- End .ratings -->
                            </div><!-- End .product-ratings -->
                        </div><!-- End .product-container -->
                        <h2 class="product-title">
                            <a href="product.html">Dress Sandals</a>
                        </h2>
                        <div class="price-box">
                            <span class="product-price">$60.00</span>
                        </div><!-- End .price-box -->

                        <div class="product-action">
                            <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                                <span>Add to Wishlist</span>
                            </a>

                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Add to Cart</span>
                            </a>

                            <a href="#" class="paction add-compare" title="Add to Compare">
                                <span>Add to Compare</span>
                            </a>
                        </div><!-- End .product-action -->
                    </div><!-- End .product-details -->
                </div><!-- End .product -->
            </div><!-- End .col-lg-3 -->

            <div class="col-6 col-md-4 col-lg-3">
                <div class="product">
                    <figure class="product-image-container">
                        <a href="product.html" class="product-image">
                            <img src="frontend/images/products/product-5.jpg" alt="product">
                        </a>
                        <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    </figure>
                    <div class="product-details">
                        <div class="ratings-container">
                            <div class="product-ratings">
                                <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                            </div><!-- End .product-ratings -->
                        </div><!-- End .product-container -->
                        <h2 class="product-title">
                            <a href="product.html">Red Highheels</a>
                        </h2>
                        <div class="price-box">
                            <span class="product-price">$79.00</span>
                        </div><!-- End .price-box -->

                        <div class="product-action">
                            <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                                <span>Add to Wishlist</span>
                            </a>

                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Add to Cart</span>
                            </a>

                            <a href="#" class="paction add-compare" title="Add to Compare">
                                <span>Add to Compare</span>
                            </a>
                        </div><!-- End .product-action -->
                    </div><!-- End .product-details -->
                </div><!-- End .product -->
            </div><!-- End .col-lg-3 -->

            <div class="col-6 col-md-4 col-lg-3">
                <div class="product">
                    <figure class="product-image-container">
                        <a href="product.html" class="product-image">
                            <img src="frontend/images/products/product-6.jpg" alt="product">
                        </a>
                        <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                        <span class="product-label label-new">New</span>
                    </figure>
                    <div class="product-details">
                        <div class="ratings-container">
                            <div class="product-ratings">
                                <span class="ratings" style="width:100%"></span><!-- End .ratings -->
                            </div><!-- End .product-ratings -->
                        </div><!-- End .product-container -->
                        <h2 class="product-title">
                            <a href="product.html">Woman Bag</a>
                        </h2>
                        <div class="price-box">
                            <span class="product-price">$110.00</span>
                        </div><!-- End .price-box -->

                        <div class="product-action">
                            <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                                <span>Add to Wishlist</span>
                            </a>

                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Add to Cart</span>
                            </a>

                            <a href="#" class="paction add-compare" title="Add to Compare">
                                <span>Add to Compare</span>
                            </a>
                        </div><!-- End .product-action -->
                    </div><!-- End .product-details -->
                </div><!-- End .product -->
            </div><!-- End .col-lg-3 -->

            <div class="col-6 col-md-4 col-lg-3">
                <div class="product">
                    <figure class="product-image-container">
                        <a href="product.html" class="product-image">
                            <img src="frontend/images/products/product-7.jpg" alt="product">
                        </a>
                        <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    </figure>
                    <div class="product-details">
                        <div class="ratings-container">
                            <div class="product-ratings">
                                <span class="ratings" style="width:20%"></span><!-- End .ratings -->
                            </div><!-- End .product-ratings -->
                        </div><!-- End .product-container -->
                        <h2 class="product-title">
                            <a href="product.html">Black Highheels</a>
                        </h2>
                        <div class="price-box">
                            <span class="product-price">$78.00</span>
                        </div><!-- End .price-box -->

                        <div class="product-action">
                            <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                                <span>Add to Wishlist</span>
                            </a>

                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Add to Cart</span>
                            </a>

                            <a href="#" class="paction add-compare" title="Add to Compare">
                                <span>Add to Compare</span>
                            </a>
                        </div><!-- End .product-action -->
                    </div><!-- End .product-details -->
                </div><!-- End .product -->
            </div><!-- End .col-lg-3 -->

            <div class="col-6 col-md-4 col-lg-3">
                <div class="product">
                    <figure class="product-image-container">
                        <a href="product.html" class="product-image">
                            <img src="frontend/images/products/product-8.jpg" alt="product">
                        </a>
                        <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    </figure>
                    <div class="product-details">
                        <div class="ratings-container">
                            <div class="product-ratings">
                                <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                            </div><!-- End .product-ratings -->
                        </div><!-- End .product-container -->
                        <h2 class="product-title">
                            <a href="product.html">Yellow Highheels</a>
                        </h2>
                        <div class="price-box">
                            <span class="product-price">$80.00</span>
                        </div><!-- End .price-box -->

                        <div class="product-action">
                            <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                                <span>Add to Wishlist</span>
                            </a>

                            <a href="product.html" class="paction add-cart" title="Add to Cart">
                                <span>Add to Cart</span>
                            </a>

                            <a href="#" class="paction add-compare" title="Add to Compare">
                                <span>Add to Compare</span>
                            </a>
                        </div><!-- End .product-action -->
                    </div><!-- End .product-details -->
                </div><!-- End .product -->
            </div><!-- End .col-lg-3 -->
        </div><!-- End .row -->

        <div class="banner banner-image mb-2 mb-md-4 mb-lg-6">
            <a href="#">
                <img src="frontend/images/banners/banner-1.jpg" alt="banner">
            </a>
        </div><!-- End .banner -->

        <div class="row">
            <div class="col-md-4">
                <div class="column-product-container">
                    <h2 class="title text-center">Featured</h2>

                    <div class="column-slider owl-carousel owl-theme">
                        <div class="product">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="frontend/images/products/home/product-1.jpg" alt="product">
                                </a>
                                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                                <span class="product-label label-new">New</span>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h2 class="product-title">
                                    <a href="product.html">Brown Casual Shirt</a>
                                </h2>
                                <div class="price-box">
                                    <span class="product-price">$68.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="frontend/images/products/home/product-2.jpg" alt="product">
                                </a>
                                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h2 class="product-title">
                                    <a href="product.html">Black Casual Shirt</a>
                                </h2>
                                <div class="price-box">
                                    <span class="old-price">$69.00</span>
                                    <span class="product-price">$59.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="frontend/images/products/home/product-3.jpg" alt="product">
                                </a>
                                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:0%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h2 class="product-title">
                                    <a href="product.html">White Casual Dress</a>
                                </h2>
                                <div class="price-box">
                                    <span class="product-price">$79.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->
                    </div><!-- End .column-slider  -->
                </div><!-- End .column-product-container -->
            </div><!-- End .col-md-4 -->

            <div class="col-md-4">
                <div class="column-product-container">
                    <h2 class="title text-center">Sales</h2>

                    <div class="column-slider owl-carousel owl-theme">
                        <div class="product">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="frontend/images/products/home/product-2.jpg" alt="product">
                                </a>
                                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h2 class="product-title">
                                    <a href="product.html">Black Casual Shirt</a>
                                </h2>
                                <div class="price-box">
                                    <span class="old-price">$69.00</span>
                                    <span class="product-price">$59.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="frontend/images/products/home/product-1.jpg" alt="product">
                                </a>
                                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                                <span class="product-label label-new">New</span>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h2 class="product-title">
                                    <a href="product.html">Brown Casual Shirt</a>
                                </h2>
                                <div class="price-box">
                                    <span class="product-price">$68.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="frontend/images/products/home/product-3.jpg" alt="product">
                                </a>
                                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:0%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h2 class="product-title">
                                    <a href="product.html">White Casual Dress</a>
                                </h2>
                                <div class="price-box">
                                    <span class="product-price">$79.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->
                    </div><!-- End .column-slider  -->
                </div><!-- End .column-product-container -->
            </div><!-- End .col-md-4 -->

            <div class="col-md-4">
                <div class="column-product-container">
                    <h2 class="title text-center">New Arrivals</h2>

                    <div class="column-slider owl-carousel owl-theme">
                        <div class="product">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="frontend/images/products/home/product-3.jpg" alt="product">
                                </a>
                                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:0%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h2 class="product-title">
                                    <a href="product.html">White Casual Dress</a>
                                </h2>
                                <div class="price-box">
                                    <span class="product-price">$79.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="frontend/images/products/home/product-1.jpg" alt="product">
                                </a>
                                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h2 class="product-title">
                                    <a href="product.html">Brown Casual Shirt</a>
                                </h2>
                                <div class="price-box">
                                    <span class="product-price">$68.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->

                        <div class="product">
                            <figure class="product-image-container">
                                <a href="product.html" class="product-image">
                                    <img src="frontend/images/products/home/product-2.jpg" alt="product">
                                </a>
                                <a href="ajax/product-quick-view.html" class="btn-quickview">Quick view</a>
                            </figure>
                            <div class="product-details">
                                <div class="ratings-container">
                                    <div class="product-ratings">
                                        <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                                    </div><!-- End .product-ratings -->
                                </div><!-- End .product-container -->
                                <h2 class="product-title">
                                    <a href="product.html">Black Casual Shirt</a>
                                </h2>
                                <div class="price-box">
                                    <span class="old-price">$69.00</span>
                                    <span class="product-price">$59.00</span>
                                </div><!-- End .price-box -->
                            </div><!-- End .product-details -->
                        </div><!-- End .product -->
                    </div><!-- End .column-slider  -->
                </div><!-- End .column-product-container -->
            </div><!-- End .col-md-4 -->
        </div><!-- End .row -->

        <div class="row">
            <div class="col-6 col-md-3">
                <div class="banner banner-image">
                    <a href="#">
                        <img src="frontend/images/banners/banner-2.jpg" alt="banner">
                    </a>
                </div><!-- End .banner -->
            </div><!-- End .col-md-3 -->

            <div class="col-6 col-md-3">
                <div class="banner banner-image">
                    <a href="#">
                        <img src="frontend/images/banners/banner-3.jpg" alt="banner">
                    </a>
                </div><!-- End .banner -->
            </div><!-- End .col-md-3 -->

            <div class="col-6 col-md-3">
                <div class="banner banner-image">
                    <a href="#">
                        <img src="frontend/images/banners/banner-4.jpg" alt="banner">
                    </a>
                </div><!-- End .banner -->
            </div><!-- End .col-md-3 -->

            <div class="col-6 col-md-3">
                <div class="banner banner-image">
                    <a href="#">
                        <img src="frontend/images/banners/banner-5.jpg" alt="banner">
                    </a>
                </div><!-- End .banner -->
            </div><!-- End .col-md-3 -->
        </div><!-- End .row -->

        <div class="mb-3"></div><!-- margin -->

        <h2 class="title text-center">Fashion Selection</h2>

        <div class="products-carousel owl-carousel owl-theme owl-nav-top">
            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-3.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:80%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Brown Scarf</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price">$66.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-6.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:0%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Women Bag</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price">$55.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-2.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    <span class="product-label label-sale">-20%</span>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:60%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Training Sneakers</a>
                    </h2>
                    <div class="price-box">
                        <span class="old-price">$101.00</span>
                        <span class="product-price">$89.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-5.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    <span class="product-label label-new">New</span>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:40%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Red Highheels</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price">$60.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-7.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Black Highheels</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price">$79.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-4.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    <span class="product-label label-new">New</span>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:100%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Running Sneakers</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price">$110.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-8.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    <span class="product-label label-sale">-20%</span>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:60%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Yellow Highheels</a>
                    </h2>
                    <div class="price-box">
                        <span class="old-price">$101.00</span>
                        <span class="product-price">$89.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-13.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    <span class="product-label label-new">New</span>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:40%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Blue Shoes</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price">$60.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-12.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:50%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Dress Sandals</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price">$79.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-10.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                    <span class="product-label label-sale">-20%</span>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:60%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Red Bag</a>
                    </h2>
                    <div class="price-box">
                        <span class="old-price">$101.00</span>
                        <span class="product-price">$89.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->

            <div class="product">
                <figure class="product-image-container">
                    <a href="product.html" class="product-image">
                        <img src="frontend/images/products/product-9.jpg" alt="product">
                    </a>
                    <a href="ajax/product-quick-view.html" class="btn-quickview">Quick View</a>
                </figure>
                <div class="product-details">
                    <div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:0%"></span><!-- End .ratings -->
                        </div><!-- End .product-ratings -->
                    </div><!-- End .product-container -->
                    <h2 class="product-title">
                        <a href="product.html">Silk Scarf</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price">$55.00</span>
                    </div><!-- End .price-box -->

                    <div class="product-action">
                        <a href="#" class="paction add-wishlist" title="Add to Wishlist">
                            <span>Add to Wishlist</span>
                        </a>

                        <a href="product.html" class="paction add-cart" title="Add to Cart">
                            <span>Add to Cart</span>
                        </a>

                        <a href="#" class="paction add-compare" title="Add to Compare">
                            <span>Add to Compare</span>
                        </a>
                    </div><!-- End .product-action -->
                </div><!-- End .product-details -->
            </div><!-- End .product -->
        </div><!-- End .featured-proucts -->

        <div class="mb-6"></div><!-- margin -->
    </div>
@endsection
