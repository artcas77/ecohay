@extends('frontend.layout.app')

@section('title' , 'Contact Us')

@section('keywords' , '')

@section('description' , '')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index-2.html"><i class="icon-home"></i></a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
            </ol>
        </div><!-- End .container -->
    </nav>

    <div class="container">
        <div id="map"></div><!-- End #map -->

        <div class="row">
            <div class="col-md-8">
                <h2 class="title">@lang('contact.write_us')</h2>
                <form action="{{ route('postContactUs') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group required-field">
                        <label for="contact-name">{{ __('contact.name') }}</label>
                        <input type="text" class="form-control" id="contact_name" name="contact_name" required>
                        @if($errors->has('contact_name'))
                            <span class="invalid-feedback">
                                {{ $errors->first('contact_name') }}
                            </span>
                        @endif
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label for="contact-email">{{ __('contact.email') }}</label>
                        <input type="email" class="form-control" id="contact_email" name="contact_email" required>
                        @if($errors->has('contact_email'))
                            <span class="invalid-feedback">
                                {{ $errors->first('contact_email') }}
                            </span>
                        @endif
                    </div><!-- End .form-group -->

                    <div class="form-group">
                        <label for="contact-phone">{{ __('contact.phone') }}</label>
                        <input type="tel" class="form-control" id="contact_phone" name="contact_phone">
                        @if($errors->has('contact_phone'))
                            <span class="invalid-feedback">
                                {{ $errors->first('contact_phone') }}
                            </span>
                        @endif
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label for="contact-message">{{ __('contact.whats') }}</label>
                        <textarea cols="30" rows="1" id="contact-message" class="form-control" name="contact_message" required></textarea>
                        @if($errors->has('contact_message'))
                            <span class="invalid-feedback">
                                {{ $errors->first('contact_message') }}
                            </span>
                        @endif
                    </div><!-- End .form-group -->

                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary">{{ __('contact.submit') }}</button>
                    </div><!-- End .form-footer -->
                </form>
            </div><!-- End .col-md-8 -->

            <div class="col-md-4">
                <h2 class="title">{{ __('contact.contact_details') }}</h2>

                <div class="contact-info">
                    <div>
                        <i class="icon-phone"></i>
                        <p><a href="tel:">0201 203 2032</a></p>
                        <p><a href="tel:">0201 203 2032</a></p>
                    </div>
                    <div>
                        <i class="icon-mobile"></i>
                        <p><a href="tel:">201-123-3922</a></p>
                        <p><a href="tel:">302-123-3928</a></p>
                    </div>
                    <div>
                        <i class="icon-mail-alt"></i>
                        <p><a href="mailto:#">porto@gmail.com</a></p>
                        <p><a href="mailto:#">porto@portotemplate.com</a></p>
                    </div>
                    <div>
                        <i class="icon-skype"></i>
                        <p>porto_skype</p>
                        <p>porto_template</p>
                    </div>
                </div><!-- End .contact-info -->
            </div><!-- End .col-md-4 -->
        </div><!-- End .row -->
    </div><!-- End .container -->

    <div class="mb-8"></div><!-- margin -->
@endsection


@section('js')
    <!-- Google Map-->
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDc3LRykbLB-y8MuomRUIY0qH5S6xgBLX4"></script>
    <script src="{{ asset('frontend/js/map.js') }}"></script>
@endsection
